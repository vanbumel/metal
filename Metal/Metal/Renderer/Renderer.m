//
//  Renderer.m
//  Metal
//
//  Created by OnSight MacBook Pro on 3/5/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import "Renderer.h"

//Frameworks
@import simd;
@import MetalKit;
@import ModelIO;
@import SceneKit.ModelIO;
#import <SVProgressHUD.h>

//Classes
#import "Mesh.h"
#import "MathUtilities.h"

//Constance
#import "ShaderTypes.h"
#import "GlobalConstance.h"

static const NSUInteger MaxBufferInFlight = 3;

@interface Renderer()

// properties
@property (strong, nonatomic) dispatch_semaphore_t inFlightSemaphore;
@property (strong, nonatomic) id<MTLDevice> device;
@property (strong, nonatomic) id<MTLCommandQueue> commandQueue;

@property (strong, nonatomic) id<MTLDepthStencilState> depthState;
@property (strong, nonatomic) MTLVertexDescriptor* mtlVertexDescriptor;
@property (assign, nonatomic) uint8_t uniformBufferIndex;
@property (assign, nonatomic) matrix_float4x4 projectionMatrix;

@property (strong, nonatomic) MTLVertexDescriptor* skyVertexDescriptor;
@property (strong, nonatomic) id<MTLTexture> skyMap;
@property (strong, nonatomic) MTKMesh* skyMesh;
@property (strong, nonatomic) id<MTLRenderPipelineState> skyboxPipelineState;
@property (strong, nonatomic) id<MTLDepthStencilState> dontWriteDepthStencilState;
@property (assign, nonatomic) matrix_float4x4 projection_matrix;

@property (strong, nonatomic) NSArray<Mesh*>* meshes;
@property (strong, nonatomic) id<MTLTexture> irradianceMap;
@property (strong, nonatomic) dispatch_group_t pipelineCreationGroup;

@property (assign, nonatomic) float scaleFactor;
@property (assign, nonatomic) BOOL safeToDraw;
@property (assign, nonatomic) QualityLevel currentQualityLevel;

@end

@implementation Renderer
{
    id<MTLBuffer> _uniformBuffers[MaxBufferInFlight];
    id<MTLRenderPipelineState> _pipelineStates[NumQualityLevels];

    id<MTLFunction> _fragmentFunction[NumQualityLevels];
    id<MTLFunction> _vertexFunction[NumQualityLevels];
}

#pragma mark - Initialization -

- (instancetype) initWithMetalKitView: (MTKView*)  mtkView
                          withObjName: (NSString*) name
                     withSkyImageName: (NSString*) imageName
{
    if (self = [super init])
    {
        _device                = mtkView.device;
        _inFlightSemaphore     = dispatch_semaphore_create(MaxBufferInFlight);
        _pipelineCreationGroup = dispatch_group_create();
        _safeToDraw            = NO;
        _currentQualityLevel   = QualityLevelHigh;
        
        [self loadMetal: mtkView];
        [self loadAssetsWithObjName: name
                   withSkyImageName: imageName];
    }
    
    return self;
}

- (MTLFunctionConstantValues*) functionConstantsForQualityLevel: (QualityLevel) quality
{
    BOOL hasBaseColorMap        = [Mesh isTextureProperty: FunctionConstantBaseColorMapIndex
                                           atQualityLevel: quality];
    BOOL hasNormalMap           = [Mesh isTextureProperty: FunctionConstantNormalMapIndex
                                           atQualityLevel: quality];
    BOOL hasMetalicMap          = [Mesh isTextureProperty: FunctionConstantMetallicMapIndex
                                           atQualityLevel: quality];
    BOOL hasRoughnessMap        = [Mesh isTextureProperty: FunctionConstantRoughnessMapIndex
                                           atQualityLevel: quality];
    BOOL hasAmbientOcclusionMap = [Mesh isTextureProperty: FunctionConstantAmbientOcclusionMapIndex
                                           atQualityLevel: quality];
    BOOL hasIrradianceMap       = [Mesh isTextureProperty: FunctionConstantIrradianceMapIndex
                                           atQualityLevel: quality];
    
    MTLFunctionConstantValues* constantValue = [MTLFunctionConstantValues new];
    
    [constantValue setConstantValue: &hasNormalMap
                               type: MTLDataTypeBool
                            atIndex: FunctionConstantNormalMapIndex];
    [constantValue setConstantValue: &hasBaseColorMap
                               type: MTLDataTypeBool
                            atIndex: FunctionConstantBaseColorMapIndex];
    [constantValue setConstantValue: &hasMetalicMap
                               type: MTLDataTypeBool
                            atIndex: FunctionConstantMetallicMapIndex];
    [constantValue setConstantValue: &hasRoughnessMap
                               type: MTLDataTypeBool
                            atIndex: FunctionConstantRoughnessMapIndex];
    [constantValue setConstantValue: &hasAmbientOcclusionMap
                               type: MTLDataTypeBool
                            atIndex: FunctionConstantAmbientOcclusionMapIndex];
    [constantValue setConstantValue: &hasIrradianceMap
                               type: MTLDataTypeBool
                            atIndex: FunctionConstantIrradianceMapIndex];
    
    return constantValue;
}

- (void) loadPipelineAsync: (MTKView*) view
{
    dispatch_queue_t pipelineQueue       = dispatch_queue_create("pipelineQueue", DISPATCH_QUEUE_CONCURRENT);
    dispatch_group_t specializationGroup = dispatch_group_create();
    
    id<MTLLibrary> defaultLibrary = [self.device newDefaultLibrary];
    
    for (uint qualityLevel = 0; qualityLevel < NumQualityLevels; qualityLevel++)
    {
        dispatch_group_enter(specializationGroup);
        
        MTLFunctionConstantValues* constantValue = [self functionConstantsForQualityLevel: qualityLevel];
        
        [defaultLibrary newFunctionWithName: @"fragmentLighting"
                             constantValues: constantValue
                          completionHandler: ^(id<MTLFunction> newFunction, NSError* error) {
                              
                              if (!newFunction)
                              {
                                  NSLog(@"Failed to specialize function, error %@", error);
                              }
                              
                              self->_fragmentFunction[qualityLevel] = newFunction;
                              dispatch_group_leave(specializationGroup);
                          }];
        
        dispatch_group_enter(specializationGroup);
        
        [defaultLibrary newFunctionWithName: @"vertexTransform"
                             constantValues: constantValue
                          completionHandler: ^(id<MTLFunction> newFunction, NSError* error) {
                              
                              if (!newFunction)
                              {
                                  NSLog(@"Failed to specialize function, error %@", error);
                              }
                              
                              self->_vertexFunction[qualityLevel] = newFunction;
                              dispatch_group_leave(specializationGroup);
                          }];
    }
    
    self.mtlVertexDescriptor = [MTLVertexDescriptor new];
    
    self.mtlVertexDescriptor.attributes[VertexAttributePosition].format      = MTLVertexFormatFloat3;
    self.mtlVertexDescriptor.attributes[VertexAttributePosition].offset      = 0;
    self.mtlVertexDescriptor.attributes[VertexAttributePosition].bufferIndex = BufferIndexMeshPositions;
    
    self.mtlVertexDescriptor.attributes[VertexAttributeTexcoord].format      = MTLVertexFormatFloat2;
    self.mtlVertexDescriptor.attributes[VertexAttributeTexcoord].offset      = 0;
    self.mtlVertexDescriptor.attributes[VertexAttributeTexcoord].bufferIndex = BufferIndexMeshGenerics;
    
    self.mtlVertexDescriptor.attributes[VertexAttributeNormal].format      = MTLVertexFormatHalf4;
    self.mtlVertexDescriptor.attributes[VertexAttributeNormal].offset      = 8;
    self.mtlVertexDescriptor.attributes[VertexAttributeNormal].bufferIndex = BufferIndexMeshGenerics;
    
    self.mtlVertexDescriptor.attributes[VertexAttributeTangent].format      = MTLVertexFormatHalf4;
    self.mtlVertexDescriptor.attributes[VertexAttributeTangent].offset      = 16;
    self.mtlVertexDescriptor.attributes[VertexAttributeTangent].bufferIndex = BufferIndexMeshGenerics;
    
    self.mtlVertexDescriptor.attributes[VertexAttributeBitangent].format      = MTLVertexFormatHalf4;
    self.mtlVertexDescriptor.attributes[VertexAttributeBitangent].offset      = 24;
    self.mtlVertexDescriptor.attributes[VertexAttributeBitangent].bufferIndex = BufferIndexMeshGenerics;
    
    self.mtlVertexDescriptor.layouts[BufferIndexMeshPositions].stride       = 12;
    self.mtlVertexDescriptor.layouts[BufferIndexMeshPositions].stepRate     = 1;
    self.mtlVertexDescriptor.layouts[BufferIndexMeshPositions].stepFunction = MTLVertexStepFunctionPerVertex;
    
    self.mtlVertexDescriptor.layouts[BufferIndexMeshGenerics].stride       = 32;
    self.mtlVertexDescriptor.layouts[BufferIndexMeshGenerics].stepRate     = 1;
    self.mtlVertexDescriptor.layouts[BufferIndexMeshGenerics].stepFunction = MTLVertexStepFunctionPerVertex;
    
    view.colorPixelFormat        = MTLPixelFormatBGRA8Unorm_sRGB;
    view.depthStencilPixelFormat = MTLPixelFormatDepth32Float_Stencil8;
    view.sampleCount             = 1;
    
    MTLRenderPipelineDescriptor* pipelineStateDescriptor = [MTLRenderPipelineDescriptor new];
    
    pipelineStateDescriptor.label                           = @"MyPipeline";
    pipelineStateDescriptor.sampleCount                     = view.sampleCount;
    pipelineStateDescriptor.vertexFunction                  = nil;
    pipelineStateDescriptor.fragmentFunction                = nil;
    pipelineStateDescriptor.vertexDescriptor                = self.mtlVertexDescriptor;
    pipelineStateDescriptor.colorAttachments[0].pixelFormat = view.colorPixelFormat;
    pipelineStateDescriptor.depthAttachmentPixelFormat      = view.depthStencilPixelFormat;
    pipelineStateDescriptor.stencilAttachmentPixelFormat    = view.depthStencilPixelFormat;
    
    {
        NSError* error;
        
        self.skyVertexDescriptor = [[MTLVertexDescriptor alloc] init];
        
        self.skyVertexDescriptor.attributes[VertexAttributePosition].format      = MTLVertexFormatFloat3;
        self.skyVertexDescriptor.attributes[VertexAttributePosition].offset      = 0;
        self.skyVertexDescriptor.attributes[VertexAttributePosition].bufferIndex = BufferIndexMeshPositions;
        
        self.skyVertexDescriptor.layouts[BufferIndexMeshPositions].stride = 12;
        
        self.skyVertexDescriptor.attributes[VertexAttributeNormal].format      = MTLVertexFormatFloat3;
        self.skyVertexDescriptor.attributes[VertexAttributeNormal].offset      = 0;
        self.skyVertexDescriptor.attributes[VertexAttributeNormal].bufferIndex = BufferIndexMeshGenerics;
        
        self.skyVertexDescriptor.layouts[BufferIndexMeshGenerics].stride = 12;
        
        id<MTLFunction> skyboxVertexFunction   = [defaultLibrary newFunctionWithName: @"skybox_vertex"];
        id<MTLFunction> skyboxFragmentFunction = [defaultLibrary newFunctionWithName: @"skybox_fragment"];
        
        MTLRenderPipelineDescriptor* renderPipelineDescriptor = [MTLRenderPipelineDescriptor new];
        
        renderPipelineDescriptor.label                           = @"Sky";
        renderPipelineDescriptor.vertexDescriptor                = self.skyVertexDescriptor;
        renderPipelineDescriptor.vertexFunction                  = skyboxVertexFunction;
        renderPipelineDescriptor.fragmentFunction                = skyboxFragmentFunction;
        renderPipelineDescriptor.colorAttachments[0].pixelFormat = view.colorPixelFormat;
        renderPipelineDescriptor.depthAttachmentPixelFormat      = view.depthStencilPixelFormat;
        renderPipelineDescriptor.stencilAttachmentPixelFormat    = view.depthStencilPixelFormat;
        
        self.skyboxPipelineState = [self.device newRenderPipelineStateWithDescriptor: renderPipelineDescriptor
                                                                               error: &error];
        
        if (!self.skyboxPipelineState)
        {
            NSLog(@"Failed to create render pipeline state, error %@", error);
        }
    }
    
    // Create depth state for post lighting operations
    {
        MTLDepthStencilDescriptor* depthStateDesc = [MTLDepthStencilDescriptor new];
     
        depthStateDesc.label                = @"Less -Writes";
        depthStateDesc                      = [[MTLDepthStencilDescriptor alloc] init];
        depthStateDesc.depthCompareFunction = MTLCompareFunctionLess;
        depthStateDesc.depthWriteEnabled    = NO;
        
        self.dontWriteDepthStencilState = [self.device newDepthStencilStateWithDescriptor: depthStateDesc];
    }
    
    dispatch_group_enter(self.pipelineCreationGroup);
    
    void (^notifyBlock)(void) = ^void()
    {
        const id<MTLDevice> device                   = self.device;
        const dispatch_group_t pipelineCreationGroup = self.pipelineCreationGroup;
        
        MTLRenderPipelineDescriptor* pipelineStateDescriptors[NumQualityLevels];
        
        dispatch_group_wait(specializationGroup, DISPATCH_TIME_FOREVER);
        
        for (uint qualityLevel = 0; qualityLevel < NumQualityLevels; qualityLevel++)
        {
            dispatch_group_enter(pipelineCreationGroup);
            
            pipelineStateDescriptors[qualityLevel] = [pipelineStateDescriptor copy];
            pipelineStateDescriptors[qualityLevel].fragmentFunction = self->_fragmentFunction[qualityLevel];
            pipelineStateDescriptors[qualityLevel].vertexFunction   = self->_vertexFunction[qualityLevel];
            
            [device newRenderPipelineStateWithDescriptor: pipelineStateDescriptors[qualityLevel]
                                       completionHandler:^(id<MTLRenderPipelineState> newPipelineState, NSError* error) {
                                          
                                           if (!newPipelineState)
                                               NSLog(@"Failed to create pipeline state, error %@", error);
                                           
                                           self->_pipelineStates[qualityLevel] = newPipelineState;
                                           dispatch_group_leave(pipelineCreationGroup);
                                       }];
        }
        
        dispatch_group_leave(pipelineCreationGroup);
    };
    
    dispatch_group_notify(specializationGroup, pipelineQueue, notifyBlock);
}

- (void) loadMetal: (MTKView*) view
{
    for (NSUInteger inFlightIndex = 0; inFlightIndex < MaxBufferInFlight; inFlightIndex++)
    {
        _uniformBuffers[inFlightIndex] = [self.device newBufferWithLength: sizeof(Uniforms)
                                                                  options: MTLResourceStorageModeShared];
    }
    
    [self loadPipelineAsync: view];
    
    MTLDepthStencilDescriptor* depthStateDesc = [MTLDepthStencilDescriptor new];
    
    depthStateDesc.depthCompareFunction = MTLCompareFunctionLess;
    depthStateDesc.depthWriteEnabled    = YES;
    
    self.depthState   = [self.device newDepthStencilStateWithDescriptor: depthStateDesc];
    self.commandQueue = [self.device newCommandQueue];
}

- (void) loadAssetsWithObjName: (NSString*) name
              withSkyImageName: (NSString*) imageName
{
    NSError* error;
    
    MDLVertexDescriptor* modelIOVertexDescriptor = MTKModelIOVertexDescriptorFromMetal(self.mtlVertexDescriptor);
    
    modelIOVertexDescriptor.attributes[VertexAttributePosition].name  = MDLVertexAttributePosition;
    modelIOVertexDescriptor.attributes[VertexAttributeTexcoord].name  = MDLVertexAttributeTextureCoordinate;
    modelIOVertexDescriptor.attributes[VertexAttributeNormal].name    = MDLVertexAttributeNormal;
    modelIOVertexDescriptor.attributes[VertexAttributeTangent].name   = MDLVertexAttributeTangent;
    modelIOVertexDescriptor.attributes[VertexAttributeBitangent].name = MDLVertexAttributeBitangent;

    NSURL* modelFileURL = [[NSBundle mainBundle] URLForResource: name
                                                  withExtension: @"obj"];
    if (!modelFileURL)
    {
        NSLog(@"Could not find model (%@) file in bundle creating specular texture",
              modelFileURL.absoluteString);
    }
    
    MTKMeshBufferAllocator* bufferAllocator = [[MTKMeshBufferAllocator alloc] initWithDevice: self.device];
    
    MDLAsset* asset = [[MDLAsset alloc] initWithURL: modelFileURL
                                   vertexDescriptor: nil
                                    bufferAllocator: bufferAllocator];
    
    vector_float3 boudingBoxMax = asset.boundingBox.maxBounds;
    
    SCNVector3 vector3 = SCNVector3FromFloat3(boudingBoxMax);
    
    float heigh = vector3.y;
    
    if (heigh > 500)
    {
        self.scaleFactor = 0.001;
    }
    else
    {
        self.scaleFactor = 1;
    }
    
    self.meshes = [Mesh newMeshesFromURL: modelFileURL
                 modelIOVertexDescriptor: modelIOVertexDescriptor
                             metalDevice: self.device
                                MDLAsset: asset
                                   error: &error];
    
    if (!self.meshes || error)
    {
        NSLog(@"Could not create meshes from model file %@", modelFileURL.absoluteString);
    }
    
    NSDictionary* textureLoaderOptions =
    @{
      MTKTextureLoaderOptionTextureUsage       : @(MTLTextureUsageShaderRead),
      MTKTextureLoaderOptionTextureStorageMode : @(MTLStorageModePrivate)
      };
    
    MTKTextureLoader* textureLoader = [[MTKTextureLoader alloc] initWithDevice: self.device];
    
    self.irradianceMap = [textureLoader newTextureWithName: @"IrradianceMap"
                                               scaleFactor: 1.0
                                                    bundle: nil
                                                   options: textureLoaderOptions
                                                     error: &error];
    
    if (!self.irradianceMap)
    {
        NSLog(@"Could not load IrradianceMap %@", error);
    }
    
    {
        MTKMeshBufferAllocator* bufferAllocator = [[MTKMeshBufferAllocator alloc] initWithDevice: self.device];
        
        MDLMesh* sphereMDLMesh = [MDLMesh newEllipsoidWithRadii: 150
                                                 radialSegments: 20
                                               verticalSegments: 20
                                                   geometryType: MDLGeometryTypeTriangles
                                                  inwardNormals: NO
                                                     hemisphere: NO
                                                      allocator: bufferAllocator];
        
        MDLVertexDescriptor* sphereDescriptor = MTKModelIOVertexDescriptorFromMetal(self.skyVertexDescriptor);
        
        sphereDescriptor.attributes[VertexAttributePosition].name = MDLVertexAttributePosition;
        sphereDescriptor.attributes[VertexAttributeNormal].name   = MDLVertexAttributeNormal;
        
        sphereMDLMesh.vertexDescriptor = sphereDescriptor;
        
        self.skyMesh = [[MTKMesh alloc] initWithMesh: sphereMDLMesh
                                              device: self.device
                                               error: &error];
        if (!self.skyMesh)
        {
            NSLog(@"Could not create mesh %@", error);
        }
    }
    
    {
        MTKTextureLoader* textureLoader = [[MTKTextureLoader alloc] initWithDevice: self.device];
        
        NSDictionary* textureLoaderOptions =
        @{
          MTKTextureLoaderOptionTextureUsage       : @(MTLTextureUsageShaderRead),
          MTKTextureLoaderOptionTextureStorageMode : @(MTLStorageModePrivate),
          };
        
        self.skyMap = [textureLoader newTextureWithName: imageName
                                            scaleFactor: 1.0
                                                 bundle: nil
                                                options: textureLoaderOptions
                                                  error: &error];
        
        if(!self.skyMap)
        {
            NSLog(@"Could not load sky texture %@", error);
        }
        
        self.skyMap.label = @"Sky Map";
    }
}

- (void) updateState: (MTKView*) view
{
    id<MTLBuffer> buffer = _uniformBuffers[self.uniformBufferIndex];
    
    Uniforms* uniforms = (Uniforms*)buffer.contents;
    
    vector_float3 directionalLightDirection = vector_normalize ((vector_float3){self.lightDirectionX, self.lightDirectionY, self.lightDirectionZ});
    
    uniforms->directionalLightInvDirection = -directionalLightDirection;
    uniforms->lightPosition                = (vector_float3){0.f};
    
    const vector_float3 xAxis             = { 1, 0, 0 };
    const vector_float3 yAxis             = { 0, 1, 0 };
    const vector_float3 zAxis             = { 0, 0, 1 };
    const matrix_float4x4 xRot            = matrix_float4x4_rotation(xAxis, self.rotationX);
    const matrix_float4x4 yRot            = matrix_float4x4_rotation(yAxis, self.rotationY);
    const matrix_float4x4 zRot            = matrix_float4x4_rotation(zAxis, self.rotationZ);
    const matrix_float4x4 scale           = matrix_float4x4_uniform_scale(self.scaleFactor);
    const matrix_float4x4 modelMatrix     = matrix_multiply(matrix_multiply(matrix_multiply(xRot, yRot), zRot), scale);
    const vector_float3 cameraTranslation = { self.poxitionX, self.positionY, self.cameraDistance };
    const matrix_float4x4 viewMatrix      = matrix_float4x4_translation(-cameraTranslation);
    
    uniforms->cameraPos                 = cameraTranslation;
    uniforms->projection_matrix         = self.projection_matrix;
    uniforms->modelMatrix               = matrix_multiply(viewMatrix, modelMatrix);
    uniforms->normalMatrix              = matrix_float4x4_extract_linear(uniforms->modelMatrix);
    uniforms->modelViewProjectionMatrix = matrix_multiply(self.projectionMatrix, uniforms->modelMatrix);
    uniforms->irradianceMapWeight       = self.globalMapWeight;
    
    const vector_float3 skyRotationAxis  = {0, 1, 0};
    const matrix_float4x4 skyModelMatrix = matrix4x4_rotation(self.skyRotation, skyRotationAxis);
    
    uniforms->sky_modelview_matrix = matrix_multiply(skyModelMatrix, skyModelMatrix);
}

- (void) drawSky: (nonnull id<MTLRenderCommandEncoder>) renderEncoder
{
    [renderEncoder pushDebugGroup: @"Draw Sky"];
    [renderEncoder setRenderPipelineState: self.skyboxPipelineState];
    [renderEncoder setDepthStencilState: self.dontWriteDepthStencilState];
    [renderEncoder setCullMode: MTLCullModeFront];
    
    [renderEncoder setVertexBuffer: _uniformBuffers[self.currentQualityLevel]
                            offset: 0
                           atIndex: BufferIndexUniforms];
    
    [renderEncoder setFragmentTexture: self.skyMap
                              atIndex: TextureIndexBaseColor];
    
    for (NSUInteger bufferIndex = 0; bufferIndex < self.skyMesh.vertexBuffers.count; bufferIndex++)
    {
        __unsafe_unretained MTKMeshBuffer* vertexBuffer = self.skyMesh.vertexBuffers[bufferIndex];
        
        if ((NSNull*)vertexBuffer != [NSNull null])
        {
            [renderEncoder setVertexBuffer: vertexBuffer.buffer
                                    offset: vertexBuffer.offset
                                   atIndex: bufferIndex];
        }
    }
    
    MTKSubmesh* sphereSubmesh = self.skyMesh.submeshes[0];
    
    [renderEncoder drawIndexedPrimitives: sphereSubmesh.primitiveType
                              indexCount: sphereSubmesh.indexCount
                               indexType: sphereSubmesh.indexType
                             indexBuffer: sphereSubmesh.indexBuffer.buffer
                       indexBufferOffset: sphereSubmesh.indexBuffer.offset];
    
    [renderEncoder popDebugGroup];
}

- (void)       mtkView: (MTKView*) view
drawableSizeWillChange: (CGSize)   size
{
    float aspect = size.width / size.height;
    float fov    = (2 * M_PI) / 5;
    float near   = 0.1;
    float far    = 100;
    
    float NearPlane = 1;
    float FarPlane  = 150;
    float fovPlane  = 65.0f * (M_PI / -180.0f);
    
    self.projectionMatrix  = matrix_float4x4_perspective(aspect, fov, near, far);
    self.projection_matrix = matrix_perspective_left_hand(fovPlane, aspect, NearPlane, FarPlane);
}

- (void) drawInMTKView: (MTKView*) view
{
    if (!self.safeToDraw)
    {
        dispatch_group_wait(self.pipelineCreationGroup, DISPATCH_TIME_FOREVER);
        self.safeToDraw = YES;
    }
    
    dispatch_semaphore_wait(self.inFlightSemaphore, DISPATCH_TIME_FOREVER);
    
    id<MTLCommandBuffer> commandBuffer = [self.commandQueue commandBuffer];
    
    commandBuffer.label = @"FunctionConstantCommand";
    
    __block dispatch_semaphore_t block_sema = self.inFlightSemaphore;
    
    [commandBuffer addCompletedHandler: ^(id<MTLCommandBuffer> buffer)
     {
         dispatch_semaphore_signal(block_sema);
     }];
    
    [self updateState: view];
    
    MTLRenderPassDescriptor* renderPassDescriptor = view.currentRenderPassDescriptor;
    
    if (renderPassDescriptor != nil)
    {
        id<MTLRenderCommandEncoder> renderEncoder = [commandBuffer renderCommandEncoderWithDescriptor: renderPassDescriptor];
        
        [self drawSky: renderEncoder];
        
        renderEncoder.label = @"FunctionConstantRenderEncoder";
        
        [renderEncoder pushDebugGroup: @"DrawMeshes"];
        
        [renderEncoder setCullMode: MTLCullModeFront];
        [renderEncoder setRenderPipelineState: _pipelineStates[self.currentQualityLevel]];
        [renderEncoder setDepthStencilState: self.depthState];
        [renderEncoder setFragmentTexture: self.irradianceMap
                                  atIndex: TextureIndexIrradianceMap];
        
        for (Mesh* mesh in self.meshes)
        {
            MTKMesh* metalKitMesh = mesh.metalKitMesh;
            
            for (NSUInteger bufferIndex = 0; bufferIndex < metalKitMesh.vertexBuffers.count; bufferIndex++)
            {
                MTKMeshBuffer* vertexBuffer = metalKitMesh.vertexBuffers[bufferIndex];
                
                if ((NSNull*)vertexBuffer != [NSNull null])
                {
                    [renderEncoder setVertexBuffer: vertexBuffer.buffer
                                            offset: vertexBuffer.offset
                                           atIndex: bufferIndex];
                }
            }
            
            for (Submesh* submesh in mesh.submeshes)
            {
                for (TextureIndex textureIndex = 0; textureIndex < NumMeshTextureIndices; textureIndex++)
                {
                    [renderEncoder setFragmentTexture: submesh.textures[textureIndex]
                                              atIndex: textureIndex];
                }
                
                [submesh computeTextureWeightsForQualityLevel: self.currentQualityLevel
                                          withGlobalMapWeight: self.globalMapWeight];
                
                [renderEncoder setFragmentBuffer: submesh.materialUniforms
                                          offset: 0
                                         atIndex: BufferIndexMaterialUniforms];
                
                MTKSubmesh* metalKitSubmesh = submesh.metalKitSubmesh;
                
                MTLViewport currentViewport = {
                    0.0,
                    0.0,
                    view.drawableSize.width,
                    view.drawableSize.height,
                    0.0,
                    1.0
                };
                    
                [renderEncoder setViewport: currentViewport];
                    
                [renderEncoder setVertexBuffer: _uniformBuffers[self.uniformBufferIndex]
                                        offset: 0
                                       atIndex: BufferIndexUniforms];
                    
                [renderEncoder setFragmentBuffer: _uniformBuffers[self.uniformBufferIndex]
                                          offset: 0
                                         atIndex: BufferIndexUniforms];
                    
                [renderEncoder drawIndexedPrimitives: metalKitSubmesh.primitiveType
                                          indexCount: metalKitSubmesh.indexCount
                                           indexType: metalKitSubmesh.indexType
                                         indexBuffer: metalKitSubmesh.indexBuffer.buffer
                                   indexBufferOffset: metalKitSubmesh.indexBuffer.offset];
            }
        }
        
        [renderEncoder popDebugGroup];
        
        [renderEncoder endEncoding];
        
        [commandBuffer presentDrawable: view.currentDrawable];
        
        [SVProgressHUD dismiss];
    }
    
    [commandBuffer commit];
}

@end

