//
//  Renderer.h
//  Metal
//
//  Created by OnSight MacBook Pro on 3/5/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MetalKit/MetalKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Renderer : NSObject

@property (assign, nonatomic) float cameraDistance;
@property (assign, nonatomic) CGFloat rotationX;
@property (assign, nonatomic) CGFloat rotationY;
@property (assign, nonatomic) CGFloat rotationZ;
@property (assign, nonatomic) float poxitionX;
@property (assign, nonatomic) float positionY;
@property (assign, nonatomic) CGFloat lightDirectionX;
@property (assign, nonatomic) CGFloat lightDirectionY;
@property (assign, nonatomic) CGFloat lightDirectionZ;
@property (assign, nonatomic) float globalMapWeight;
@property (assign, nonatomic) float skyRotation;

- (instancetype) initWithMetalKitView: (MTKView*)  mtkView
                          withObjName: (NSString*) name
                     withSkyImageName: (NSString*) imageName;

- (void) drawInMTKView: (MTKView*) view;

- (void)       mtkView: (MTKView*) view
drawableSizeWillChange: (CGSize)   size;

@end

NS_ASSUME_NONNULL_END
