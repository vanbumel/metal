//
//  ViewController.m
//  Metal
//
//  Created by OnSight MacBook Pro on 3/4/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import "MetalKitViewController.h"

//Frameworks
@import MetalKit;

//Classes
#import "Renderer.h"

//Constance
#import "GlobalConstance.h"

@interface MetalKitViewController () <MTKViewDelegate, UIGestureRecognizerDelegate>

//outlets
@property (weak, nonatomic) IBOutlet MTKView* mtkView;
@property (weak, nonatomic) IBOutlet UIButton* xButton;
@property (weak, nonatomic) IBOutlet UIButton* yButton;
@property (weak, nonatomic) IBOutlet UIButton* zButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem* rotateButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem* lightButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem* mapWeightButton;
@property (weak, nonatomic) IBOutlet UISlider* topSlider;
@property (weak, nonatomic) IBOutlet UISlider* leftSlider;
@property (weak, nonatomic) IBOutlet UISlider* rightSlider;
@property (weak, nonatomic) IBOutlet UISlider* mapWeightSlider;
@property (weak, nonatomic) IBOutlet UILabel* nameFeature;
@property (weak, nonatomic) IBOutlet UILabel* nearLabel;
@property (weak, nonatomic) IBOutlet UILabel* midLable;
@property (weak, nonatomic) IBOutlet UILabel* farLabel;
@property (weak, nonatomic) IBOutlet UILabel* xAxis;
@property (weak, nonatomic) IBOutlet UILabel* yAxis;
@property (weak, nonatomic) IBOutlet UILabel* zAxis;
@property (strong, nonatomic) IBOutlet UIPanGestureRecognizer* panGesture;

//properties
@property (strong, nonatomic) Renderer* renderer;
@property (strong, nonatomic) NSString* objName;
@property (strong, nonatomic) NSString* imageName;
@property (assign, nonatomic) float baseZoomFactor, pinchZoomFactor;
@property (assign, nonatomic) CGPoint angularVelocity;
@property (assign, nonatomic) CGPoint angle;
@property (strong, nonatomic) NSTimer* timer;
@property (assign, nonatomic) CGFloat rotationX;
@property (assign, nonatomic) CGFloat rotationY;
@property (assign, nonatomic) CGFloat rotationZ;
@property (assign, nonatomic) CGFloat positionX;
@property (assign, nonatomic) CGFloat positionY;
@property (assign, nonatomic) CGFloat lightDirectionX;
@property (assign, nonatomic) CGFloat lightDirectionY;
@property (assign, nonatomic) CGFloat lightDirectionZ;
@property (assign, nonatomic) CGFloat mapWeight;
@property (assign, nonatomic) BOOL skyRotationEnable;

@end

@implementation MetalKitViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self setupDefaultValues];
    
    self.mtkView.device   = MTLCreateSystemDefaultDevice();
    self.mtkView.delegate = self;
    
    self.renderer = [[Renderer alloc] initWithMetalKitView: self.mtkView
                                               withObjName: self.objName
                                          withSkyImageName: self.imageName];
    
    [self mtkView: self.mtkView drawableSizeWillChange: self.mtkView.drawableSize];
    
    self.leftSlider.transform  = CGAffineTransformMakeRotation(M_PI_2);
    self.rightSlider.transform = CGAffineTransformMakeRotation(M_PI_2);
    
    self.panGesture.delegate = self;
    
    [self buildButtons];
}

- (void) dealloc
{
    [self.timer invalidate];
    
    self.timer    = nil;
    self.renderer = nil;
    self.mtkView  = nil;
}


#pragma mark - Public methods -

- (void) configureControllerWith: (NSString*) name
                    withSkyImage: (NSString*) imageName
{
    self.objName   = name;
    self.imageName = imageName;
}


#pragma mark - MTKViewDelegate methods -

- (void)       mtkView: (MTKView*) view
drawableSizeWillChange: (CGSize)   size
{
    [self.renderer mtkView: view
    drawableSizeWillChange: size];
}

- (void) drawInMTKView: (MTKView*) view
{
    self.renderer.cameraDistance  = self.baseZoomFactor * self.pinchZoomFactor;
    self.renderer.rotationX       = self.rotationX;
    self.renderer.rotationY       = self.rotationY;
    self.renderer.rotationZ       = self.rotationZ;
    self.renderer.poxitionX       = self.positionX;
    self.renderer.positionY       = self.positionY;
    self.renderer.lightDirectionX = self.lightDirectionX;
    self.renderer.lightDirectionY = self.lightDirectionY;
    self.renderer.lightDirectionZ = self.lightDirectionZ;
    self.renderer.globalMapWeight = self.mapWeight;
    
    if (self.skyRotationEnable)
    {
        self.renderer.skyRotation += 0.001;
    }

    [self.renderer drawInMTKView: view];
}


#pragma mark - Gesture pich to zoom method -

- (IBAction) pinchGestureRecognize: (UIPinchGestureRecognizer*) sender
{
    switch (sender.state)
    {
        case UIGestureRecognizerStateChanged:
            self.pinchZoomFactor = 1 / sender.scale;
            break;
        case UIGestureRecognizerStateEnded:
        {
            self.baseZoomFactor = self.baseZoomFactor * self.pinchZoomFactor;
            
            self.pinchZoomFactor = 1;
        }
            break;
            
        default:
            break;
    }
    
    float constrainedZoom = fmax(1.0, fmin(200.0, self.baseZoomFactor * self.pinchZoomFactor));
    
    self.pinchZoomFactor = constrainedZoom / self.baseZoomFactor;
}


#pragma mark - Pan gesture method -

- (IBAction) panGestureRecognizer: (UIPanGestureRecognizer*) sender
{
    CGPoint velocity = [sender velocityInView: self.mtkView];
    
    self.angularVelocity = CGPointMake(velocity.x * kVelocityScale, velocity.y * kVelocityScale);
    
    if (sender.state == UIGestureRecognizerStateBegan)
    {
        self.timer = [NSTimer scheduledTimerWithTimeInterval: 0.01
                                                      target: self
                                                    selector: @selector(updateValues)
                                                    userInfo: nil
                                                     repeats: true];
    }
    else
        if(sender.state == UIGestureRecognizerStateEnded)
        {
            [self.timer invalidate];
            self.timer = nil;
        }
    
    [self updateMotionTimestep: self.timer.timeInterval];
}

- (BOOL) gestureRecognizer: (UIGestureRecognizer*) gestureRecognizer
        shouldReceiveTouch: (UITouch*)             touch
{
    if ([touch.view isKindOfClass: [UISlider class]])
    {
        return NO;
    }
    
    return YES;
}


#pragma mark - UISlider action methods -

- (IBAction) onTopSliderChangeValue: (UISlider*) sender
{
    self.lightDirectionZ = sender.value;
}

- (IBAction) onLeftSliderChangeValue: (UISlider*) sender
{
    self.lightDirectionX = sender.value;
}

- (IBAction) onRightSliderChangeValue: (UISlider*) sender
{
    self.lightDirectionY = sender.value;
}

- (IBAction) onMapWeightSliderChangeValue: (UISlider*) sender
{
    self.mapWeight = sender.value;
}


#pragma mark - LongPressGesture method -

- (void) longPressHandler: (UILongPressGestureRecognizer*) gesture
{
    if (gesture.view == self.xButton)
    {
        [self gestureWithSelector: @selector(onXButtonPressed:)
                       withSender: gesture];
    }
    else
        if (gesture.view == self.yButton)
        {
            [self gestureWithSelector: @selector(onYButtonPressed:)
                           withSender: gesture];
        }
    else
        if (gesture.view == self.zButton)
        {
            [self gestureWithSelector: @selector(onZButtonPressed:)
                           withSender: gesture];
        }
}


#pragma mark - Buttons Actions -

- (IBAction) onBackButtonPressed: (UIBarButtonItem*) sender
{
    [self.timer invalidate];

    self.timer    = nil;
    self.renderer = nil;
    self.mtkView  = nil;
    
    [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction) onXButtonPressed: (UIButton*) sender
{
    self.rotationX -= .02;
}

- (IBAction) onYButtonPressed: (UIButton*) sender
{
    self.rotationY += .02;
}

- (IBAction) onZButtonPressed: (UIButton*) sender
{
    self.rotationZ += .02;
}

- (IBAction) onStopAnimationButtonPressed: (UIBarButtonItem*) sender
{
    sender.style = (sender.style == UIBarButtonItemStyleDone) ? UIBarButtonItemStylePlain : UIBarButtonItemStyleDone;
    
    NSString* imageName = (sender.style == UIBarButtonItemStyleDone) ? @"playIcon" : @"pauseIcon";
    
    [sender setImage: [UIImage imageNamed: imageName]];
    
    if (sender.style == UIBarButtonItemStylePlain)
    {
        self.skyRotationEnable = YES;
    }
    else
    {
        self.skyRotationEnable = NO;
    }
}

- (IBAction) onRotateButtonPressed: (UIBarButtonItem*) sender
{
    sender.style = (sender.style == UIBarButtonItemStylePlain) ? UIBarButtonItemStyleDone : UIBarButtonItemStylePlain;
    
    self.xButton.hidden = (sender.style == UIBarButtonItemStyleDone) ? NO : YES;
    self.yButton.hidden = (sender.style == UIBarButtonItemStyleDone) ? NO : YES;
    self.zButton.hidden = (sender.style == UIBarButtonItemStyleDone) ? NO : YES;
    
    NSString* nameFeature = @"Rotate";
    
    if (sender.style == UIBarButtonItemStyleDone)
    {
        self.nameFeature.text = [self addTextForNameFeature: nameFeature];
    }
    else
    {
        self.nameFeature.text = [self removeNameFeature: nameFeature
                                            withOldText: self.nameFeature.text];
    }
    
    NSString* imageName = (sender.style == UIBarButtonItemStylePlain) ? @"rotate" :  @"close";
    
    [self.rotateButton setImage: [UIImage imageNamed: imageName]];
}

- (IBAction) onLightButtonPressed: (UIBarButtonItem*) sender
{
    sender.style = (sender.style == UIBarButtonItemStylePlain) ? UIBarButtonItemStyleDone : UIBarButtonItemStylePlain;
    
    self.topSlider.hidden   = (sender.style == UIBarButtonItemStyleDone) ? NO : YES;
    self.leftSlider.hidden  = (sender.style == UIBarButtonItemStyleDone) ? NO : YES;
    self.rightSlider.hidden = (sender.style == UIBarButtonItemStyleDone) ? NO : YES;
    
    self.xAxis.hidden = (sender.style == UIBarButtonItemStyleDone) ? NO : YES;
    self.yAxis.hidden = (sender.style == UIBarButtonItemStyleDone) ? NO : YES;
    self.zAxis.hidden = (sender.style == UIBarButtonItemStyleDone) ? NO : YES;
    
    NSString* nameFeature = @"Light position";
    
    if (sender.style == UIBarButtonItemStyleDone)
    {
        self.nameFeature.text = [self addTextForNameFeature: nameFeature];
    }
    else
    {
        self.nameFeature.text = [self removeNameFeature: nameFeature
                                            withOldText: self.nameFeature.text];
    }
    
    NSString* imageName = (sender.style == UIBarButtonItemStylePlain) ? @"light" : @"close";
    
    [self.lightButton setImage: [UIImage imageNamed: imageName]];
}

- (IBAction) onShowMapWeightSlider: (UIBarButtonItem*) sender
{
    sender.style = (sender.style == UIBarButtonItemStylePlain) ? UIBarButtonItemStyleDone : UIBarButtonItemStylePlain;
    
    self.mapWeightSlider.hidden = (sender.style == UIBarButtonItemStyleDone) ? NO : YES;
    
    NSString* nameFeature = @"Quality level";
    
    if (sender.style == UIBarButtonItemStyleDone)
    {
        self.nameFeature.text = [self addTextForNameFeature: nameFeature];
    }
    else
    {
        self.nameFeature.text = [self removeNameFeature: nameFeature
                                            withOldText: self.nameFeature.text];
    }
    
    self.nearLabel.hidden = (sender.style == UIBarButtonItemStyleDone) ? NO : YES;
    self.midLable.hidden  = (sender.style == UIBarButtonItemStyleDone) ? NO : YES;
    self.farLabel.hidden  = (sender.style == UIBarButtonItemStyleDone) ? NO : YES;
    
    NSString* imageName = (sender.style == UIBarButtonItemStylePlain) ? @"FillColor" :  @"close";
    
    [self.mapWeightButton setImage: [UIImage imageNamed: imageName]];
}


#pragma mark - Internal methods -

- (NSArray*) buttonsWithLongPressProperty
{
    return @[self.xButton, self.yButton, self.zButton];
}

- (void) buildButtons
{
    NSArray* buttonsArray = [self buttonsWithLongPressProperty];
    
    [buttonsArray enumerateObjectsUsingBlock:^(UIButton* button, NSUInteger idx, BOOL * _Nonnull stop) {
        
        UILongPressGestureRecognizer* buttonPress = [[UILongPressGestureRecognizer alloc] initWithTarget: self
                                                                                                  action: @selector(longPressHandler:)];
        [button addGestureRecognizer: buttonPress];
        
        buttonPress.delegate             = self;
        buttonPress.minimumPressDuration = 0.01;
        buttonPress.allowableMovement    = 0.01;
    }];
}

- (void) updateMotionTimestep: (NSTimeInterval) duration
{
    if ( duration > 0)
    {
        self.angle = CGPointMake(self.angle.x + self.angularVelocity.x * duration,
                                 self.angle.y + self.angularVelocity.y * duration);
                
        self.angularVelocity = CGPointMake(self.angularVelocity.x * (1 - kDamping),
                                           self.angularVelocity.y * (1 - kDamping));
    }
}

- (void) updateValues
{
    self.positionX = self.angle.x;
    self.positionX = -self.angle.x;
    self.positionY = -self.angle.y;
    self.positionY = self.angle.y;
}

- (void) setupDefaultValues
{
    self.baseZoomFactor    = 4;
    self.pinchZoomFactor   = 1;
    self.mapWeight         = 1;
    self.rotationX         = 0.0;
    self.rotationY         = 0.0;
    self.rotationZ         = 0.0;
    self.lightDirectionX   = 0.0;
    self.lightDirectionY   = -6.0;
    self.lightDirectionZ   = -6.0;
    self.skyRotationEnable = NO;
}

- (NSString*) addTextForNameFeature: (NSString*) name
{
    NSString* textLabel;
    
    if (self.nameFeature.text.length == 0)
    {
        textLabel = [NSString stringWithFormat: @"%@", name];
    }
    else
    {
        textLabel = [NSString stringWithFormat: @"%@, %@", self.nameFeature.text, name];
    }
    
    return textLabel;
}

- (NSString*) removeNameFeature: (NSString*) term
                    withOldText: (NSString*) text
{
    NSString* newText;
    
    NSString* firstCase  = [NSString stringWithFormat: @", %@", term];
    NSString* secondCase = [NSString stringWithFormat: @"%@, ", term];
    
    NSRange firsRange   = [text rangeOfString : firstCase];
    NSRange secondRange = [text rangeOfString : secondCase];
    
    if (firsRange.location != NSNotFound)
    {
        newText = [text stringByReplacingOccurrencesOfString: firstCase
                                                  withString: @""];
    }
    else if (secondRange.location != NSNotFound)
    {
        newText = [text stringByReplacingOccurrencesOfString: secondCase
                                                  withString: @""];
    }
    else
    {
        newText = [text stringByReplacingOccurrencesOfString: term
                                                  withString: @""];
    }
    
    return newText;
}

- (void) gestureWithSelector: (SEL)                           selector
                  withSender: (UILongPressGestureRecognizer*) gesture
{
    if(gesture.state == UIGestureRecognizerStateBegan)
    {
        if (self.timer.isValid) {
            [self.timer invalidate];
        }
        
        self.timer = [NSTimer scheduledTimerWithTimeInterval: 0.01
                                                      target: self
                                                    selector: selector
                                                    userInfo: nil
                                                     repeats: true];
    }
    else
        if(gesture.state == UIGestureRecognizerStateEnded)
        {
            [self.timer invalidate];
            self.timer = nil;
        }
}

@end
