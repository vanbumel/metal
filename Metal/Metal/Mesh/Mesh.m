//
//  Mesh.m
//  Metal
//
//  Created by OnSight MacBook Pro on 3/4/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

@import MetalKit;
@import ModelIO;

#import "Mesh.h"
#import "MathUtilities.h"

@interface Submesh()

// properties
@property (strong, nonatomic) NSMutableArray<id<MTLTexture>>* textures;
@property (assign, nonatomic) MaterialUniforms* uniforms;
@property (strong, nonatomic) id<MTLBuffer> materialUniforms;

@end

@implementation Submesh

+ (nonnull id<MTLTexture>) createMetalTextureFromMaterial: (nonnull MDLMaterial*)      material
                                  modelIOMaterialSemantic: (MDLMaterialSemantic)       materialSemantic
                                      modelIOMaterialType: (MDLMaterialPropertyType)   defaultPropertyType
                                    metalKitTextureLoader: (nonnull MTKTextureLoader*) textureLoader
                                          materialUniform: (nullable void*)            uniform
{
    id<MTLTexture> texture = nil;

    NSArray<MDLMaterialProperty *> *propertiesWithSemantic = [material propertiesWithSemantic: materialSemantic];

    for (MDLMaterialProperty* property in propertiesWithSemantic)
    {
        assert(property.semantic == materialSemantic);

        if (property.type == MDLMaterialPropertyTypeString)
        {
            NSDictionary* textureLoaderOptions = @{
                                                   MTKTextureLoaderOptionTextureUsage       : @(MTLTextureUsageShaderRead),
                                                   MTKTextureLoaderOptionTextureStorageMode : @(MTLStorageModePrivate)
                                                   };

            NSMutableString* URLString = [[NSMutableString alloc] initWithString: @"file://"];

            [URLString appendString: property.stringValue];

            NSURL* textureURL = [NSURL URLWithString: URLString];

            texture = [textureLoader newTextureWithContentsOfURL: textureURL
                                                         options: textureLoaderOptions
                                                           error: nil];
            
            if (texture)
            {
                continue;
            }

            NSString* lastComponent = [property.stringValue componentsSeparatedByString: @"/"].lastObject;

            texture = [textureLoader newTextureWithName: lastComponent
                                            scaleFactor: 1.0
                                                 bundle: nil
                                                options: textureLoaderOptions
                                                  error: nil];

            if (texture)
            {
                continue;
            }

            [NSException raise: @"Texture data for material property not found"
                        format: @"Requested material property semantic: %lu string: %@", materialSemantic, property.stringValue];
        }
        else
            if (uniform && defaultPropertyType != MDLMaterialPropertyTypeNone && property.type == defaultPropertyType)
        {
            switch (defaultPropertyType)
            {
                case MDLMaterialPropertyTypeFloat:
                    *(float*)uniform = property.floatValue;
                    break;
                case MDLMaterialPropertyTypeFloat3:
                    *(vector_float3*)uniform = property.float3Value;
                    break;
                default:
                    [NSException raise: @"Invalid MDLMaterialPropertyType for semantic"
                                format: @"Requested MDLMaterialPropertyType(%lu) for material property semantic(%lu)", defaultPropertyType, materialSemantic];
            }
        }
    }

    if (!texture)
    {
        [NSException raise: @"No appropriate material property from which to create texture"
                    format: @"Requested material property semantic: %lu", materialSemantic];
    }

    return texture;
}

- (nonnull instancetype) initWithModelIOSubmesh: (nonnull MDLSubmesh*)       modelIOSubmesh
                                metalKitSubmesh: (nonnull MTKSubmesh*)       metalKitSubmesh
                          metalKitTextureLoader: (nonnull MTKTextureLoader*) textureLoader
{
    if (self = [super init])
    {
        _metalKitSubmesh = metalKitSubmesh;

        _textures = [[NSMutableArray alloc] initWithCapacity: NumMeshTextureIndices];

        for (NSUInteger shaderIndex = 0; shaderIndex < NumMeshTextureIndices; shaderIndex++)
        {
            [_textures addObject: (id<MTLTexture>)[NSNull null]];
        }

        _materialUniforms = [textureLoader.device newBufferWithLength: sizeof(MaterialUniforms)
                                                              options: 0];

        if (!_materialUniforms)
        {
            [NSException raise: @"Could not create uniform buffer"
                        format: @""];
        }

        _uniforms = (MaterialUniforms*)self.materialUniforms.contents;

        _uniforms->baseColor        = (vector_float3){1.0, 1.0, 1.0};
        _uniforms->roughness        = 0.2f;
        _uniforms->metalness        = 0;
        _uniforms->ambientOcclusion = 0.5f;
        _uniforms->irradiatedColor  = (vector_float3){1.0, 1.0, 1.0};
        
        _textures[TextureIndexBaseColor] = [Submesh createMetalTextureFromMaterial: modelIOSubmesh.material
                                                           modelIOMaterialSemantic: MDLMaterialSemanticBaseColor
                                                               modelIOMaterialType: MDLMaterialPropertyTypeFloat3
                                                             metalKitTextureLoader: textureLoader
                                                                   materialUniform: &(_uniforms->baseColor)];

        _textures[TextureIndexMetallic] = [Submesh createMetalTextureFromMaterial: modelIOSubmesh.material
                                                          modelIOMaterialSemantic: MDLMaterialSemanticMetallic
                                                              modelIOMaterialType: MDLMaterialPropertyTypeFloat3
                                                            metalKitTextureLoader: textureLoader
                                                                  materialUniform: &(_uniforms->metalness)];

        _textures[TextureIndexRoughness] = [Submesh createMetalTextureFromMaterial: modelIOSubmesh.material
                                                           modelIOMaterialSemantic: MDLMaterialSemanticRoughness
                                                               modelIOMaterialType: MDLMaterialPropertyTypeFloat3
                                                             metalKitTextureLoader: textureLoader
                                                                   materialUniform: &(_uniforms->roughness)];

        _textures[TextureIndexNormal] = [Submesh createMetalTextureFromMaterial: modelIOSubmesh.material
                                                        modelIOMaterialSemantic: MDLMaterialSemanticTangentSpaceNormal
                                                            modelIOMaterialType: MDLMaterialPropertyTypeNone
                                                          metalKitTextureLoader: textureLoader
                                                                materialUniform: nil];

        _textures[TextureIndexAmbientOcclusion] = [Submesh createMetalTextureFromMaterial: modelIOSubmesh.material
                                                                  modelIOMaterialSemantic: MDLMaterialSemanticAmbientOcclusion
                                                                      modelIOMaterialType: MDLMaterialPropertyTypeNone
                                                                    metalKitTextureLoader: textureLoader
                                                                          materialUniform: nil];
    }

    return self;
}

- (FunctionConstant) mapTextureBindPointToFunctionConstantIndex: (TextureIndex) textureIndex
{
    switch (textureIndex)
    {
        case TextureIndexBaseColor:
            return FunctionConstantBaseColorMapIndex;
        case TextureIndexNormal:
            return FunctionConstantNormalMapIndex;
        case TextureIndexMetallic:
            return FunctionConstantMetallicMapIndex;
        case TextureIndexAmbientOcclusion:
            return FunctionConstantAmbientOcclusionMapIndex;
        case TextureIndexRoughness:
            return FunctionConstantRoughnessMapIndex;
        default:
            assert(false);
    }
}

- (void) computeTextureWeightsForQualityLevel: (QualityLevel) quality
                          withGlobalMapWeight: (float)        globalWeight
{
    for (TextureIndex textureIndex = 0; textureIndex < NumMeshTextureIndices; textureIndex++)
    {
        _uniforms->mapWeights[textureIndex] = globalWeight;
    }
}

@end

@interface Mesh()

// properties
@property (strong, nonatomic) NSMutableArray<Submesh*>* submeshes;

@end

@implementation Mesh

- (nonnull instancetype) initWithModelIOMesh: (nonnull MDLMesh*)               modelIOMesh
                    modelIOVertexDescription: (nonnull MDLVertexDescriptor*)   vertexDescriptor
                       metalKitTextureLoader: (nonnull MTKTextureLoader*)      textureLoader
                                 metalDevice: (nonnull id<MTLDevice>)          device
                                       error: (NSError* __nullable* __nullable) error
{
    if (self = [super init])
    {
        [modelIOMesh addNormalsWithAttributeNamed: MDLVertexAttributeNormal
                                  creaseThreshold: 0.1];
                
        [modelIOMesh addTangentBasisForTextureCoordinateAttributeNamed: MDLVertexAttributeTextureCoordinate
                                                  normalAttributeNamed: MDLVertexAttributeNormal
                                                 tangentAttributeNamed: MDLVertexAttributeTangent];
                
        [modelIOMesh addOrthTanBasisForTextureCoordinateAttributeNamed: MDLVertexAttributeTextureCoordinate
                                                  normalAttributeNamed: MDLVertexAttributeTangent
                                                 tangentAttributeNamed: MDLVertexAttributeBitangent];
                
        modelIOMesh.vertexDescriptor = vertexDescriptor;

        MTKMesh* metalKitMesh = [[MTKMesh alloc] initWithMesh: modelIOMesh
                                                       device: device
                                                        error: error];

        _metalKitMesh = metalKitMesh;

        assert(metalKitMesh.submeshes.count == modelIOMesh.submeshes.count);

        _submeshes = [[NSMutableArray alloc] initWithCapacity: metalKitMesh.submeshes.count];

        [metalKitMesh.submeshes enumerateObjectsUsingBlock:^(MTKSubmesh * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            Submesh* submesh = [[Submesh alloc] initWithModelIOSubmesh: modelIOMesh.submeshes[idx]
                                                       metalKitSubmesh: metalKitMesh.submeshes[idx]
                                                 metalKitTextureLoader: textureLoader];
            
            [self->_submeshes addObject: submesh];
        }];
    }

    return self;
}

+ (NSArray<Mesh*>*) newMeshesFromObject: (nonnull MDLObject*)              object
               modelIOVertexDescription: (nonnull MDLVertexDescriptor*)    vertexDescriptor
                  metalKitTextureLoader: (nonnull MTKTextureLoader*)       textureLoader
                            metalDevice: (nonnull id<MTLDevice>)           device
                                  error: (NSError* __nullable* __nullable) error
{
    NSMutableArray<Mesh*>* newMeshes = [[NSMutableArray alloc] init];

    if ([object isKindOfClass: [MDLMesh class]])
    {
        MDLMesh* mesh = (MDLMesh*) object;

        Mesh* newMesh = [[Mesh alloc] initWithModelIOMesh: mesh
                                 modelIOVertexDescription: vertexDescriptor
                                    metalKitTextureLoader: textureLoader
                                              metalDevice: device
                                                    error: error];

        [newMeshes addObject: newMesh];
    }

    for (MDLObject* child in object.children)
    {
        NSArray<Mesh*>* childMeshes;

        childMeshes = [Mesh newMeshesFromObject: child
                       modelIOVertexDescription: vertexDescriptor
                          metalKitTextureLoader: textureLoader
                                    metalDevice: device
                                          error: error];

        [newMeshes addObjectsFromArray: childMeshes];
    }

    return newMeshes;
}

+ (NSArray<Mesh*>*) newMeshesFromURL: (NSURL*)                              url
             modelIOVertexDescriptor: (MDLVertexDescriptor*)                vertexDescriptor
                         metalDevice: (id<MTLDevice>)                       device
                            MDLAsset: (MDLAsset*)                           asset
                               error: (NSError* __autoreleasing _Nullable*) error
{
    if (!asset)
    {
        NSLog(@"Failed to open model file with given URL: %@", url.absoluteString);
        return nil;
    }
    
    MTKTextureLoader* textureLoader = [[MTKTextureLoader alloc] initWithDevice: device];

    NSMutableArray<Mesh*>* newMeshes = [[NSMutableArray alloc] init];
    
    for (MDLObject* object in asset)
    {
        NSArray<Mesh*>* assetMeshes;

        assetMeshes = [Mesh newMeshesFromObject: object
                       modelIOVertexDescription: vertexDescriptor
                          metalKitTextureLoader: textureLoader
                                    metalDevice: device
                                          error: error];

        [newMeshes addObjectsFromArray: assetMeshes];
    }

    return newMeshes;
}

+ (BOOL) isTextureProperty: (FunctionConstant) propertyIndex
            atQualityLevel: (QualityLevel)     quality
{
    QualityLevel minLevelForProperty = QualityLevelHigh;

    switch (propertyIndex)
    {
        case FunctionConstantBaseColorMapIndex:
        case FunctionConstantIrradianceMapIndex:
            minLevelForProperty = QualityLevelMedium;
            break;
        default:
            break;
    }

    return quality <= minLevelForProperty;
}

@end
