//
//  Mesh.h
//  Metal
//
//  Created by OnSight MacBook Pro on 3/4/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MetalKit/MetalKit.h>
#import <simd/simd.h>

//Utilities
#import "ShaderTypes.h"

NS_ASSUME_NONNULL_BEGIN

@interface Submesh : NSObject

@property (nonatomic, readonly) MTKSubmesh* metalKitSubmesh;
@property (nonatomic, readonly) NSArray<id<MTLTexture>>* textures;
@property (nonatomic, readonly) id<MTLBuffer> materialUniforms;

- (void) computeTextureWeightsForQualityLevel: (QualityLevel) quality
                          withGlobalMapWeight: (float)        globalWeight;

@end

@interface Mesh : NSObject

@property (nonatomic, readonly) MTKMesh* metalKitMesh;
@property (nonatomic, readonly) NSArray<Submesh*>* submeshes;

+ (nullable NSArray<Mesh*>*) newMeshesFromURL: (nonnull NSURL*)                  url
                      modelIOVertexDescriptor: (nonnull MDLVertexDescriptor*)    vertexDescriptor
                                  metalDevice: (nonnull id<MTLDevice>)           device
                                     MDLAsset: (nonnull MDLAsset*)               asset
                                        error: (NSError* __nullable* __nullable) error;

+ (BOOL) isTextureProperty: (FunctionConstant) propertyIndex
            atQualityLevel: (QualityLevel)     quality;

@end

NS_ASSUME_NONNULL_END
