//
//  ShaderTypes.h
//  Metal
//
//  Created by OnSight MacBook Pro on 3/4/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#ifndef ShaderTypes_h
#define ShaderTypes_h

#include <simd/simd.h>

// Buffer index values shared between shader and C code to ensure Metal shader buffer inputs match
//   Metal API buffer set calls
typedef enum BufferIndex
{
    BufferIndexMeshPositions    = 0,
    BufferIndexMeshGenerics     = 1,
    BufferIndexUniforms         = 2,
    BufferIndexMaterialUniforms = 3
} BufferIndex;

// Attribute index values shared between shader and C code to ensure Metal shader vertex
//   attribute indices match the Metal API vertex descriptor attribute indices
typedef enum VertexAttribute
{
    VertexAttributePosition  = 0,
    VertexAttributeTexcoord  = 1,
    VertexAttributeNormal    = 2,
    VertexAttributeTangent   = 3,
    VertexAttributeBitangent = 4
} VertexAttribute;

// Texture index values shared between shader and C code to ensure Metal shader texture indices
//   match indices of Metal API texture set calls
typedef enum TextureIndex
{
    TextureIndexBaseColor        = 0,
    TextureIndexMetallic         = 1,
    TextureIndexRoughness        = 2,
    TextureIndexNormal           = 3,
    TextureIndexAmbientOcclusion = 4,
    TextureIndexIrradianceMap    = 5,
    NumMeshTextureIndices = TextureIndexAmbientOcclusion+1,
} TextureIndex;

typedef enum FunctionConstant
{
    FunctionConstantBaseColorMapIndex,
    FunctionConstantNormalMapIndex,
    FunctionConstantMetallicMapIndex,
    FunctionConstantRoughnessMapIndex,
    FunctionConstantAmbientOcclusionMapIndex,
    FunctionConstantIrradianceMapIndex
} FunctionConstant;

typedef enum QualityLevel
{
    QualityLevelHigh   = 0,
    QualityLevelMedium = 1,
    QualityLevelLow    = 2,
    NumQualityLevels
} QualityLevel;

// Structure shared between shader and C code to ensure the layout of uniform data accessed in
//    Metal shaders matches the layout of uniform data set in C code
typedef struct
{
    // Per Frame Uniforms
    vector_float3 cameraPos;
    matrix_float4x4 projection_matrix;
    
    // Per Mesh Uniforms
    matrix_float4x4 modelMatrix;
    matrix_float4x4 modelViewProjectionMatrix;
    matrix_float3x3 normalMatrix;
    
    matrix_float4x4 sky_modelview_matrix;
    
    // Per Light Properties
    vector_float3 directionalLightInvDirection;
    vector_float3 lightPosition;
    
    vector_float3 irradiatedColor;
    float irradianceMapWeight;
} Uniforms;

typedef struct
{
    vector_float3 baseColor;
    vector_float3 irradiatedColor;
    vector_float3 roughness;
    vector_float3 metalness;
    float         ambientOcclusion;
    float         mapWeights[NumMeshTextureIndices];
} MaterialUniforms;

#endif /* ShaderTypes_h */

