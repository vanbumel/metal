//
//  GlobalConstance.h
//  Metal
//
//  Created by OnSight MacBook Pro on 3/8/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#ifndef GlobalConstance_h
#define GlobalConstance_h

typedef NS_ENUM(NSUInteger, RotationState)
{
    SliderState,
    GestureState,
};

static const CGFloat kVelocityScale = 0.01;
static const CGFloat kDamping       = 0.05;

typedef NS_ENUM(NSUInteger, ViewControllerState)
{
    MetalKitViewControllerState,
    SceneKitViewControllerState,
};

#endif /* GlobalConstance_h */
