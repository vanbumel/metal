//
//  AppDelegate.h
//  Metal
//
//  Created by OnSight MacBook Pro on 3/4/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

