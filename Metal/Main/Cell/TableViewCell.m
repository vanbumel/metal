//
//  TableViewCell.m
//  MetalSampler
//
//  Created by OnSight MacBook Pro on 2/12/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import "TableViewCell.h"

@interface TableViewCell()

// properties
@property (weak, nonatomic) IBOutlet UILabel* titleLabel;

@end

@implementation TableViewCell

- (void) fillContent: (NSString*) name
{
    self.titleLabel.text = name;
}

@end
