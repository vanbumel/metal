//
//  ViewController.m
//  MetalSampler
//
//  Created by OnSight MacBook Pro on 2/12/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import "MainViewController.h"

//Classes
#import "DataDisplayManager.h"
#import "MainControllerModel.h"
#import "MetalKitViewController.h"
#import <SVProgressHUD.h>
#import "SceneBackgroundViewController.h"

//Constance
#import "GlobalConstance.h"

@interface MainViewController ()

//Outlets
@property (weak, nonatomic) IBOutlet UITableView* tableView;
@property (weak, nonatomic) IBOutlet UIImageView* imageView;

//Properties
@property (strong, nonatomic) DataDisplayManager* displayManager;
@property (strong, nonatomic) MainControllerModel* model;
@property (strong, nonatomic) NSString* titleLable;
@property (strong, nonatomic) NSString* imageName;

@end

@implementation MainViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self bindingUI];
}


#pragma mark - Properties -

- (DataDisplayManager*) displayManager
{
    if (_displayManager == nil)
    {
        _displayManager = [[DataDisplayManager alloc] initWithDataSource: self.model
                                                              withOutput: self];
    }
    
    return _displayManager;
}

- (MainControllerModel *)model
{
    if (_model == nil)
    {
        _model = [MainControllerModel new];
    }
    
    return _model;
}


#pragma mark - DataDisplayManagerOutput methods -

- (void) didSelectRowWithName: (NSString*) name
{
    self.titleLable = name;
    
    [SVProgressHUD showWithStatus: @"Please wait file opens..."];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier: @"ShowMetalKitViewControllerID"
                                  sender: nil];
    });
}


#pragma mark - Actions -

- (IBAction) onTapGesturePressed: (UITapGestureRecognizer*) sender
{
    [self performSegueWithIdentifier: @"ShowSceneBackgroundSegueID"
                              sender: nil];
}


#pragma mark - Segue -

- (void) prepareForSegue: (UIStoryboardSegue*) segue
                  sender: (id)                 sender
{
    if ( [segue.identifier isEqualToString: @"ShowMetalKitViewControllerID"] )
    {
        MetalKitViewController* controller = segue.destinationViewController;
        
        NSString* imageName = [self removeNameFeature: self.imageName];
        
        [controller configureControllerWith: self.titleLable
                               withSkyImage: imageName];
    }
    else if ( [segue.identifier isEqualToString: @"ShowSceneBackgroundSegueID"] )
    {
        SceneBackgroundViewController* controller = segue.destinationViewController;
        
        controller.output = self;
    }
}


#pragma mark - SceneBackgroundOutput methods -

- (void) fillContentWithImageName: (NSString*) imageName
{
    self.imageName = imageName;
    
    self.imageView.image = [UIImage imageNamed: imageName];
}


#pragma mark - Internal methods -

- (void) bindingUI
{
    self.tableView.dataSource = self.displayManager;
    self.tableView.delegate   = self.displayManager;
    
    self.imageName = @"SkyGrey1";
    
    UIImage* image = [UIImage imageNamed: self.imageName];
    
    self.imageView.image = image;
}

- (NSString*) removeNameFeature: (NSString*) term
{
    NSString* newText;
    
    NSString* firstCase = [NSString stringWithFormat: @"1"];
    
    NSRange firsRange = [term rangeOfString: firstCase];
    
    if (firsRange.location != NSNotFound)
    {
        newText = [term stringByReplacingOccurrencesOfString: firstCase
                                                  withString: @""];
    }
    
    return newText;
}

@end
