//
//  ViewController.h
//  MetalSampler
//
//  Created by OnSight MacBook Pro on 2/12/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import <UIKit/UIKit.h>

//Protocols
#import "DataDisplayManagerOutput.h"
#import "SceneBackgroundOutput.h"

@interface MainViewController : UIViewController <DataDisplayManagerOutput, SceneBackgroundOutput>


@end

