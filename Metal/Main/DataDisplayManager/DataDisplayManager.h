//
//  DataDisplayManager.h
//  MetalSampler
//
//  Created by OnSight MacBook Pro on 2/12/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import <Foundation/Foundation.h>

//Frameworks
#import <UIKit/UIKit.h>

//Protocols
@protocol DataDisplayManagerInput;
@protocol DataDisplayManagerOutput;

NS_ASSUME_NONNULL_BEGIN

@interface DataDisplayManager : NSObject <UITableViewDataSource, UITableViewDelegate>

- (instancetype) initWithDataSource: (id<DataDisplayManagerInput>)  dataSource
                         withOutput: (id<DataDisplayManagerOutput>) output;

@end

NS_ASSUME_NONNULL_END
