//
//  DataDisplayManager.m
//  MetalSampler
//
//  Created by OnSight MacBook Pro on 2/12/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import "DataDisplayManager.h"

//Classes
#import "TableViewCell.h"

//Protocols
#import "DataDisplayManagerInput.h"
#import "DataDisplayManagerOutput.h"

@interface DataDisplayManager()

// properties
@property (strong, nonatomic) NSArray* content;

@property (weak, nonatomic) id<DataDisplayManagerInput> dataSource;
@property (weak, nonatomic) id<DataDisplayManagerOutput> output;

@end

@implementation DataDisplayManager


#pragma mark - Initialization -

- (instancetype) initWithDataSource: (id<DataDisplayManagerInput>)  dataSource
                         withOutput: (id<DataDisplayManagerOutput>) output
{
    if (self = [super init])
    {
        self.dataSource = dataSource;
        self.output     = output;
    }
    
    return self;
}

- (NSArray*) content
{
    if (_content == nil)
    {
        _content = [self.dataSource fetchTableContent];
    }
    
    return _content;
}


#pragma mark - UITableViewDataSource methods -

- (NSInteger) tableView: (UITableView*) tableView
  numberOfRowsInSection: (NSInteger)    section
{
    return self.content.count;
}

- (UITableViewCell*) tableView: (UITableView*) tableView
         cellForRowAtIndexPath: (NSIndexPath*) indexPath
{
    TableViewCell* cell = [tableView dequeueReusableCellWithIdentifier: @"cellID"
                                                          forIndexPath: indexPath];
    
    [cell fillContent: self.content[indexPath.row]];
    
    return cell;
}


#pragma mark - UITableViewDelegate methods -

- (void)      tableView: (UITableView*) tableView
didSelectRowAtIndexPath: (NSIndexPath*) indexPath
{
    [tableView deselectRowAtIndexPath: indexPath
                             animated: YES];
    
    [self.output didSelectRowWithName: self.content[indexPath.row]];
}


@end
