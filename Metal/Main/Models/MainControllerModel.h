//
//  MainControllerModel.h
//  MetalSampler
//
//  Created by OnSight MacBook Pro on 2/12/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import <Foundation/Foundation.h>

//protocols
#import "DataDisplayManagerInput.h"

NS_ASSUME_NONNULL_BEGIN

@interface MainControllerModel : NSObject <DataDisplayManagerInput>

@end

NS_ASSUME_NONNULL_END
