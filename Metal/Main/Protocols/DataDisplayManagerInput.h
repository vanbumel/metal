//
//  DataDisplayManagerInput.h
//  MetalSampler
//
//  Created by OnSight MacBook Pro on 2/12/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol DataDisplayManagerInput <NSObject>

- (NSArray*) fetchTableContent;

@end

NS_ASSUME_NONNULL_END
