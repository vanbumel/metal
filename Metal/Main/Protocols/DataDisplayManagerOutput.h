//
//  DataDisplayManagerOutput.h
//  MetalSampler
//
//  Created by OnSight MacBook Pro on 2/12/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol DataDisplayManagerOutput <NSObject>

- (void) didSelectRowWithName: (NSString*) name;

@end

NS_ASSUME_NONNULL_END
