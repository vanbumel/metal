//
//  main.m
//  Metal
//
//  Created by OnSight MacBook Pro on 3/4/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
