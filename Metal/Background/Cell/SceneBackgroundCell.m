//
//  SceneBackgroundCell.m
//  Metal
//
//  Created by OnSight MacBook Pro on 4/24/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import "SceneBackgroundCell.h"

@interface SceneBackgroundCell()

// properties
@property (weak, nonatomic) IBOutlet UIImageView* imageView;

@end

@implementation SceneBackgroundCell

- (void) configureCellWith: (NSString*) imageName
{
    self.imageView.image = [UIImage imageNamed: imageName];
}

@end
