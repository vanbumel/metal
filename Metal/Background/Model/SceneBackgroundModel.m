//
//  SceneBackgroundModel.m
//  Metal
//
//  Created by OnSight MacBook Pro on 4/24/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import "SceneBackgroundModel.h"

@implementation SceneBackgroundModel

- (NSArray*) fetchCollectionViewContent
{
    return @[@"SkyGrey1", @"SkyIrradianceMap1", @"SkyMaskonaive1", @"SkyMaskonaive21", @"SkyNissiBeach1", @"SkyNissiBeach21", @"SkyStorforsen21", @"SkyTeide1", @"SkyTexture1", @"SkyYokohama1"];
}

@end
