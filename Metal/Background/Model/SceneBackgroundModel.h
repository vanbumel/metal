//
//  SceneBackgroundModel.h
//  Metal
//
//  Created by OnSight MacBook Pro on 4/24/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SceneBackgroundDDMInput.h"

NS_ASSUME_NONNULL_BEGIN

@interface SceneBackgroundModel : NSObject <SceneBackgroundDDMInput>

@end

NS_ASSUME_NONNULL_END
