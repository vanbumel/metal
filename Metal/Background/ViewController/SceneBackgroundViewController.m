//
//  SceneBackgroundViewController.m
//  Metal
//
//  Created by OnSight MacBook Pro on 4/24/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import "SceneBackgroundViewController.h"

//Classes
#import "SceneBackgroundDataDisplayManager.h"
#import "SceneBackgroundModel.h"

@interface SceneBackgroundViewController () <SceneBackgroundDDMOutput>
//Outlets
@property (weak, nonatomic) IBOutlet UICollectionView* collectionView;

//properties
@property (strong, nonatomic) SceneBackgroundModel* model;
@property (strong, nonatomic) SceneBackgroundDataDisplayManager* displayManager;

@end

@implementation SceneBackgroundViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self configureUI];
}


#pragma mark - Properties -

- (SceneBackgroundModel *)model
{
    if (_model == nil)
    {
        _model = [SceneBackgroundModel new];
    }
    
    return _model;
}

- (SceneBackgroundDataDisplayManager *)displayManager
{
    if (_displayManager == nil)
    {
        _displayManager = [[SceneBackgroundDataDisplayManager alloc] initWithDataSource: self.model
                                                                             withOutput: self];
    }
    
    return _displayManager;
}


#pragma mark - SceneBackgroundOutput methods -

- (void) didSelectCellWith: (NSString*) imageName
{
    [self.output fillContentWithImageName: imageName];
    
    [self.navigationController popViewControllerAnimated: YES];
}


#pragma mark - Binding UI -

- (void) configureUI
{
    self.collectionView.dataSource = self.displayManager;
    self.collectionView.delegate   = self.displayManager;
}



@end
