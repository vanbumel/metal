//
//  SceneBackgroundViewController.h
//  Metal
//
//  Created by OnSight MacBook Pro on 4/24/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import <UIKit/UIKit.h>

// Protocols
#import "SceneBackgroundOutput.h"

NS_ASSUME_NONNULL_BEGIN

@interface SceneBackgroundViewController : UIViewController

@property (weak, nonatomic) id<SceneBackgroundOutput> output;

@end

NS_ASSUME_NONNULL_END
