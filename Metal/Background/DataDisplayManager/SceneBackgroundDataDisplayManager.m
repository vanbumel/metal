//
//  SceneBackgroundDataDisplayManager.m
//  Metal
//
//  Created by OnSight MacBook Pro on 4/24/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import "SceneBackgroundDataDisplayManager.h"

#import "SceneBackgroundCell.h"

@interface SceneBackgroundDataDisplayManager()

// properties
@property (weak, nonatomic) id<SceneBackgroundDDMOutput> output;
@property (weak, nonatomic) id<SceneBackgroundDDMInput> dataSource;

@property (strong, nonatomic) NSArray* contentArray;

@end

@implementation SceneBackgroundDataDisplayManager


#pragma mark - Public methods -

- (instancetype) initWithDataSource: (id<SceneBackgroundDDMInput>)  dataSource
                         withOutput: (id<SceneBackgroundDDMOutput>) output
{
    if (self == [super init])
    {
        self.dataSource = dataSource;
        self.output     = output;
    }
    
    return self;
}

- (NSArray*) contentArray
{
    if (_contentArray == nil)
    {
        _contentArray = [self.dataSource fetchCollectionViewContent];
    }
    
    return _contentArray;
}


#pragma mark - UICollectionViewDataSource methods -

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger) collectionView: (UICollectionView*) collectionView
      numberOfItemsInSection: (NSInteger)         section
{
    return self.contentArray.count;
}

- (UICollectionViewCell*) collectionView: (UICollectionView*) collectionView
                  cellForItemAtIndexPath: (NSIndexPath*)      indexPath
{
    SceneBackgroundCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"collectionCell"
                                                                          forIndexPath: indexPath];
    
    [cell configureCellWith: self.contentArray[indexPath.row]];
    
    return cell;
}


#pragma mark - UICollectionViewDelegate methods -

- (void)  collectionView: (UICollectionView*) collectionView
didSelectItemAtIndexPath: (NSIndexPath*)      indexPath
{
    [self.output didSelectCellWith: self.contentArray[indexPath.row]];
}

@end
