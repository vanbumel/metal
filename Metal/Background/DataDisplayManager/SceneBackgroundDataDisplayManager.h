//
//  SceneBackgroundDataDisplayManager.h
//  Metal
//
//  Created by OnSight MacBook Pro on 4/24/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import <UIKit/UIKit.h>

//protocols
#import "SceneBackgroundDDMInput.h"
#import "SceneBackgroundDDMOutput.h"

NS_ASSUME_NONNULL_BEGIN

@interface SceneBackgroundDataDisplayManager : NSObject <UICollectionViewDataSource, UICollectionViewDelegate>

- (instancetype) initWithDataSource: (id<SceneBackgroundDDMInput>)  dataSource
                         withOutput: (id<SceneBackgroundDDMOutput>) output;

@end

NS_ASSUME_NONNULL_END
